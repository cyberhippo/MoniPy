# --------------------------
# Imports

import time
from colorama import Fore

# --------------------------

def alerting(results):
    if 80 > results['availability'][1] and results['availability'][0] > time.time() - 120:
        print(Fore.RED + "ALERT")
        print("Website " + results["websiteUrl"] + " is down. availability=" + str(results['availability'][1]) + ", time=" + str(time.time()))
    if results['availability'][1] >= 80 and  results['availability'][0] > time.time() - 120 :
        print(Fore.GREEN + "ALERT")
        print("Website " + results["websiteUrl"] + " is up. availability=" + str(results['availability'][1]) + ", time=" + str(time.time()))
