[![Build Status](https://travis-ci.org/GitHippo/MoniPy.svg?branch=master)](https://travis-ci.org/GitHippo/MoniPy)
[![Coverage Status](https://coveralls.io/repos/github/GitHippo/MoniPy/badge.svg)](https://coveralls.io/github/GitHippo/MoniPy)

# MoniPy
Website monitoring tool in Python

![Monitoring](http://www.navantis.com/wp-content/uploads/2015/01/MangagedMonitoringConsole.png)

## Instructions

* Make sure to activate the virtualenv

`
. datadog/bin/activate
`

* To run the monitoring tool

`
python monitoring.py
`

* To run the test on the dummy server

`
python local.py &
`
<br>
`
python monitoring.py
`

## Screenshots

### Basic use

![basic](img/data.png)

### Different stats of timeframes

![stats](img/10m.png)

### Alert down

![alert down](img/down.png)

### Alert up

![alert up](img/upalert.png)

### Localhost testing

![local testing](img/localhost.png)

## Mentions

* ![**Travis CI**](https://travis-ci.org/) for the continuous integration.
* ![**Coveralls**](https://coveralls.io/) for the code coverage.
