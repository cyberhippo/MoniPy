# --------------------------
# Imports

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import threading
import urllib3
import time
import urllib.request

# --------------------------

# Pool manager to handle the requests
http = urllib3.PoolManager()

# Disable warnings in requests' vendored urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Computing response code
def computeResponseCode(websiteUrl, interval):
    try:
        response = http.request('GET', websiteUrl)
    except urllib3.exceptions.NewConnectionError:
        print('Connection failed for ' + websiteUrl)
    return response.status


# Computing response Time
def computeResponseTime(websiteUrl, interval):
    try:
        responseTime = requests.get(str(websiteUrl)).elapsed.total_seconds()
    except requestError:
        print('Connection failed for ' + websiteUrl)
    return responseTime


# Global function to compute metrics
def compute(data):

    # Extracting values from user input data
    websiteUrl = data[0]
    interval = data[1]

    # Computing response code
    responseCode = computeResponseCode(websiteUrl, interval)

    # Computing response time
    responseTime = computeResponseTime(websiteUrl, interval)

    return [time.time(), responseCode, responseTime]
