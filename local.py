#!/usr/bin/python

# Source : https://docs.python.org/3.7/library/http.server.html?highlight=http%20server#module-http.server

# --------------------------
# Imports

from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from random import randint

# --------------------------

# Port number to run the dummy local server
PORT_NUMBER = 80


#This class will handles any incoming request from the browser
class myHandler(BaseHTTPRequestHandler):

	#Handler for the GET requests
    def do_GET(self):

        # Random number to generate an error code 50% of the time
        temp = randint(0,4)
        if temp % 2 == 0 :
            self.send_response(500)
        else :
            self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
		# Send the html message
        self.wfile.write("Hello World !")
        return

try:
	#Create a web server and define the handler to manage the
	#incoming request
	server = HTTPServer(('', PORT_NUMBER), myHandler)
	print('Started httpserver on port ' + str(PORT_NUMBER))

	#Wait forever for incoming htto requests
	server.serve_forever()

except KeyboardInterrupt:
	print ("^C received, shutting down the web server")
	server.socket.close()
