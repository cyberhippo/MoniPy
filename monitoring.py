#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Console program to monitor performance and availability of websites
'''

# --------------------------
# Imports

from datetime import datetime
from colorama import Fore, Back, Style
from compute import *
from stats import *
from alerting import *
import threading

# --------------------------

# Function to compute metrics and report results according to the user input

def compute_metrics(data):

    # Thread to repeat the function
    threading.Timer(data[1], compute_metrics, args=(data,)).start()

    # Function to compute the metrics for each website
    metrics = compute(data)

    # Appending the metrics to the results
    results["responseCode"].append([metrics[0],metrics[1]])
    results["responseTime"].append([metrics[0],metrics[2]])

    # Pretty reporting of the results
    availability = report(results)

    results["availability"] = availability

    # Alerting if necessary
    alert = alerting(results)

    # Append the alerting results for history purpose
    results["alerting"].append([time.time(),alert])




# User input for the website urls to monitor (an array)

userInput = input("Enter the URLs to monitor and the check intervals in seconds as follow [['https://www.datadoghq.com', 60], ['https://github.com', 300], ['https://www.google.com', 180]] : ")

# userInput = [['https://www.datadoghq.com', 5]]

# Local testing of dummy http server on localhost
# userInput = [['http://localhost/', 3]]

print(Fore.RED + "\n-----------------------------")
print(Fore.WHITE + "Website list and intervals" )
print(Fore.RED + "-----------------------------")
for data in userInput:
    print(Fore.BLUE + data[0] +" - " + str(data[1]) + " sec")
print(Fore.RED + "-----------------------------\n\n")

# Go through all user inputs and process it

for data in userInput:

    # Data structure to store the computing results
    results = { "websiteUrl" : data[0],
                "interval" : data[1],
                "responseCode" : [],
                "responseTime" : [],
                "availability" : 0,
                "alerting" : []}

    compute_metrics(data)
