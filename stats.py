# --------------------------
# Imports

import os
import time
from colorama import Fore

# --------------------------

# Function to report the results from the compute function

def report(results):

    # Flushing old results
    os.system('cls||clear')

    # Generic Intro
    print(Fore.RED + "-----------------------------")
    print(Fore.WHITE + "Computing metrics for " + Fore.BLUE + results['websiteUrl'] + Fore.WHITE + " every " + Fore.BLUE + str(results['interval']) + " sec.")

    # timeframe 2 minutes

    print(Fore.RED + "-----------------------------")
    print(Fore.WHITE + "Timeframe : "+ Fore.BLUE +" 2 minutes")

    ## Reporting max response time
    maxResponseTime = results['responseTime'][-1][1]
    for responseTime in results['responseTime']:
        if responseTime[0] > time.time() - 120 and responseTime[1] > maxResponseTime:
            maxResponseTime = responseTime[1]
    print(Fore.WHITE + "Max Response Time : " + Fore.GREEN + str(maxResponseTime))

    ## Reporting avg response time
    avgResponseTime = 0
    count = 0
    for responseTime in results['responseTime']:
        if responseTime[0] > time.time() - 120 :
            avgResponseTime = avgResponseTime + responseTime[1]
            count = count + 1
    avgResponseTime = avgResponseTime/count

    print(Fore.WHITE + "Avg Response Time : " + Fore.GREEN + str(avgResponseTime))

    ## Reporting availability and code count
    count = 0
    count200 = 0
    count500 = 0
    for responseCode in results['responseCode']:
        if responseCode[0] > time.time() - 120 and responseCode[1] == 200 :
            count200 = count200 + 1
            count = count + 1
        if responseCode[0] > time.time() - 120 and responseCode[1] == 500 :
            count500 = count500 + 1
            count = count + 1
    availability = count200 / count * 100
    print(Fore.WHITE + "Count code 200 : " + Fore.GREEN + str(count200))
    print(Fore.WHITE + "Count code 500 : " + Fore.GREEN + str(count500))
    print(Fore.WHITE + "Availability : " + Fore.GREEN + str(availability) + " %")


    # timeframe 10 minutes

    print(Fore.RED + "-----------------------------")
    print(Fore.WHITE + "Timeframe : "+ Fore.BLUE +" 10 minutes")

    ## Reporting max response time

    maxResponseTime = results['responseTime'][-1][1]
    for responseTime in results['responseTime']:
        if responseTime[0] > time.time() - 600 and responseTime[1] > maxResponseTime:
            maxResponseTime = responseTime[1]
    print(Fore.WHITE + "Max Response Time : " + Fore.GREEN + str(maxResponseTime))

    ## Reporting avg response time
    avgResponseTime = 0
    count = 0
    for responseTime in results['responseTime']:
        if responseTime[0] > time.time() - 600 :
            avgResponseTime = avgResponseTime + responseTime[1]
            count = count + 1
    avgResponseTime = avgResponseTime/count

    print(Fore.WHITE + "Avg Response Time : " + Fore.GREEN + str(avgResponseTime))

    ## Reporting availability
    count = 0
    count200 = 0
    count500 = 0
    for responseCode in results['responseCode']:
        if responseCode[0] > time.time() - 600 and responseCode[1] == 200 :
            count200 = count200 + 1
            count = count + 1
        if responseCode[0] > time.time() - 600 and responseCode[1] == 500 :
            count500 = count500 + 1
            count = count + 1
    availability = count200 / count * 100
    print(Fore.WHITE + "Count code 200 : " + Fore.GREEN + str(count200))
    print(Fore.WHITE + "Count code 500 : " + Fore.GREEN + str(count500))
    print(Fore.WHITE + "Availability : " + Fore.GREEN + str(availability) + " %")


    # Generic Outro
    print(Fore.RED + "-----------------------------")

    return [time.time(),availability]
